const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')
const Secret = require('../../config/keys').jwtSecret
const pool = require('../../db');

router.post('/sign_up', async (req, res) => {
    const {
        email,
        password,
        firstName,
        lastName
    } = req.body;

    if (!email || !password || !firstName || !lastName) {
        return res.status(400).json({
            msg: 'Please enter all fields'
        });
    }

    const hashedPassword = bcrypt.hashSync(password, 5);
    const role_type_id = 1;
    const created_at = new Date();

    pool.query(
        'SELECT * from users WHERE email = $1',
        [email], (err, result) => {
            if (err) {
                return res.status(500).json({
                    msg: err.message
                });
            }

            if (result.rows.length > 0) {
                return res.status(400).json({
                    msg: 'User already exist'
                });
            } else
                pool.query(
                    'INSERT INTO users (firstName, lastName, email, password, role_type_id, created_at ) VALUES ($1, $2, $3, $4, $5, $6) RETURNING email, password ',
                    [firstName, lastName, email, hashedPassword, role_type_id, created_at])
                .then(message => res.status(200).json(message))
                .then(message=>console.log(message))
                .catch(err => res.status(500).json({
                    message: err.message,
                }));
        });
});

router.post('/sign_in', async (req, res) => {
    const {
        email,
        password
    } = req.body;

    if (!email || !password) {
        return res.status(400).json({
            msg: 'Please enter all fields'
        });
    }

    pool.query('SELECT * FROM users WHERE email = $1',
        [email], (err, result) => {
            if (err) {
                return res.status(500).json({
                    msg: err.message
                });
            }

            console.log(result.rows[0].email);
            bcrypt.compare(password, result.rows[0].password)
                .then(matched => {
                    if (!matched) {
                        return res.status(400).json({
                            message: 'Wrong credentials'
                        });
                    } else {
                        const token = jwt.sign({
                            userId: result.id
                        }, Secret, {
                            expiresIn: '3h'
                        });
                        res.status(200).json({
                            token,
                            user: result.rows[0]
                        });
                    }
                });
        });
});




module.exports = router;