const express = require('express')
const router = express.Router()
const pool = require('../../db');

router.get('/', (req, res) => {
    pool.query(`SELECT * FROM users`)
     .then(result=> res.json(result))
     .catch(err => res.status(500).json(err.message))
     
    });


router.get('/current', (req, res) => {
    const {
        email,
    } = req.body;
    pool.query(`SELECT * FROM users WHERE email = $1`), [email]
        .then(res => {
            console.log(res.rows[0])
        })
        .catch(err => {
            console.error(err);
        });

});





module.exports = router;