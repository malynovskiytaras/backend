const express = require('express');
const pool = require('./db');
const auth = require('./routes/api/auth');
const users = require ('./routes/api/users');


const app = express();
app.use(express.json());


app.use('/api/auth', auth);
app.use('/api/users', users);

const port = process.env.PORT || 5000;
app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))